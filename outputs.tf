# output "load_balancer_sg_id" {
#   description = "Id of backend-sg"
#   value       = aws_security_group.load_balancer[0].id
# }

# output "backend_tg_arn" {
#   description = "Id of backend-sg"
#   value       = aws_lb_target_group.backend[0].arn
# }

# output "frontend_tg_arn" {
#   description = "Id of backend-sg"
#   value       = aws_lb_target_group.frontend[0].arn
# }

output "loadbalancer_dns_name" {
  description = "Id of backend-sg"
  value       = aws_lb.load_balancer.dns_name
}

output "loadbalancer_zone_id" {
  description = "Id of backend-sg"
  value       = aws_lb.load_balancer.zone_id
}
