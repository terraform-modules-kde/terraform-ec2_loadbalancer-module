#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "aws_region" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# VPC VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "vpc_id" {
  type = string
}

variable "subnet_public_id" {
  type    = string
  default = ""
}

variable "subnet_public_id2" {
  type    = string
  default = ""
}

#-----------------------------------------------------------------------------------------------------
# EC2 Instances VARIABLES
#-----------------------------------------------------------------------------------------------------

# variable "instance_frontend_id" {
#   type = string
# }

# variable "instance_backend_id" {
#   type = string
# }

# variable "instance_rabbit_id" {
#   type = string
# }
#-----------------------------------------------------------------------------------------------------
# EC2 LOAD BALANCER VARIABLES
#-----------------------------------------------------------------------------------------------------
variable "load_balancer_name" {
  type = string
}

variable "load_balancer_security_group_id" {
  type = string
}

variable "load_balancer_internal" {
  type    = bool
  default = false
}

variable "load_balancer_type" {
  type    = string
  default = "application"
}

variable "load_balancer_enable_deletion_protection" {
  type    = bool
  default = false
}

variable "loadbalancer_access_logs" {
  default = {}
}

variable "target_group_instance_attachment" {
  default = {}
}

variable "target_groups" {
  default = {}
}

variable "listeners" {
  default = {}
}

variable "rules" {
  default = {}
}

variable "instances" {
  default = {}
}

#-----------------------------------------------------------------------------------------------------
# ROUTE 53 VARIABLES
#-----------------------------------------------------------------------------------------------------

# variable "domain_name" {
#   type    = string
#   default = "*"
# }

variable "certificate_arn" {
  type = string
}

