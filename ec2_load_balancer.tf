#-----------------------------------------------------------------------------------------------------
# EC2 LOAD BALANCER
#-----------------------------------------------------------------------------------------------------
resource "aws_lb" "load_balancer" {
  name                       = var.load_balancer_name
  internal                   = var.load_balancer_internal
  load_balancer_type         = var.load_balancer_type
  security_groups            = [var.load_balancer_security_group_id]
  subnets                    = [var.subnet_public_id, var.subnet_public_id2]
  enable_deletion_protection = var.load_balancer_enable_deletion_protection

  dynamic "access_logs" {
    for_each = var.loadbalancer_access_logs

    content {
      bucket  = loadbalancer_access_logs.value.bucket
      prefix  = loadbalancer_access_logs.value.prefix
      enabled = loadbalancer_access_logs.value.enabled
    }
  }

  tags = {
    Name        = "${var.project_name}-${var.environment}-load_balancer"
    Project     = var.project_name
    Environment = var.environment
  }
}

resource "aws_lb_target_group" "this_tg" {
  for_each    = var.target_groups
  name        = lookup(each.value, "target_group_name", "${var.project_name}-${var.environment}-${each.key}")
  port        = lookup(each.value, "target_group_port", 80)
  protocol    = lookup(each.value, "target_group_protocol", "HTTP")
  vpc_id      = var.vpc_id
  target_type = lookup(each.value, "target_group_target_type", "instance")

  dynamic "health_check" {
    for_each = lookup(each.value, "target_group_health_check", {})
    content {
      interval            = health_check.value.interval
      protocol            = health_check.value.protocol
      path                = health_check.value.path
      port                = health_check.value.port
      timeout             = health_check.value.timeout
      healthy_threshold   = health_check.value.healthy_threshold
      unhealthy_threshold = health_check.value.unhealthy_threshold
      matcher             = health_check.value.matcher
    }
  }
}

resource "aws_lb_target_group_attachment" "this_instance_attachment" {
  for_each         = var.target_group_instance_attachment
  target_group_arn = aws_lb_target_group.this_tg[each.value.target_group_aliase].arn
  target_id        = var.instances[each.value.instance_aliase]
}

resource "aws_lb_listener" "this_listener" {
  for_each          = var.listeners
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = lookup(each.value, "listener_port", 80)
  protocol          = lookup(each.value, "listener_protocol", "HTTP")
  ssl_policy        = lookup(each.value, "ssl_policy", "")
  certificate_arn   = lookup(each.value, "listener_protocol", "HTTP") =="HTTPS" ? var.certificate_arn : ""

  dynamic "default_action" {
    for_each = lookup(each.value, "listener_default_action_redirect", {})

    content {
      type = default_action.value.listener_default_action_type

      redirect {
        port        = default_action.value.listener_default_action_port
        protocol    = default_action.value.listener_default_action_protocol
        status_code = default_action.value.listener_default_action_status_code
      }
    }
  }

  dynamic "default_action" {
    for_each = lookup(each.value, "listener_default_action_forward", {})

    content {
      type             = default_action.value.listener_default_action_type
      target_group_arn = aws_lb_target_group.this_tg[default_action.value.listener_target_group_aliase].arn
    }
  }
}


resource "aws_lb_listener_rule" "this_rule" {
  for_each     = var.rules
  listener_arn = aws_lb_listener.this_listener[each.value.listener_aliase].arn
  priority     = lookup(each.value, "priority", 5)

  dynamic "action" {
    for_each = lookup(each.value, "rule_action_redirect", {})

    content {
      type = action.value.listener_default_action_type

      redirect {
        port        = action.value.listener_default_action_port
        protocol    = action.value.listener_default_action_protocol
        status_code = action.value.listener_default_action_status_code
      }
    }
  }

  dynamic "action" {
    for_each = lookup(each.value, "rule_action_forward", {})

    content {
      type             = action.value.listener_default_action_type
      target_group_arn = aws_lb_target_group.this_tg[action.value.listener_target_group_aliase].arn
    }
  }
  condition {
    dynamic "host_header" {
      for_each = lookup(each.value, "host_header_condition", {})

      content {
        values = host_header.value.values
      }
    }
  }
}
